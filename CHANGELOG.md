# Changelog


#### BalzersGauge-1.1 - 04/03/19:
    As request by vacuum group, GazFactor as been removed

#### BalzersGauge-1.0 - 04/03/19:
    Initial Revision in GIT
