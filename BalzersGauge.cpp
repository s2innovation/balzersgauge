/*----- PROTECTED REGION ID(BalzersGauge.cpp) ENABLED START -----*/
static const char *RcsId = "$Id: BalzersGauge.cpp,v 1.20 2011/09/19 10:55:00 pascal_verdier Exp $";
//=============================================================================
//
// file :        BalzersGauge.cpp
//
// description : C++ source for the BalzersGauge and its commands.
//               The class is derived from Device. It represents the
//               CORBA servant object which will be accessed from the
//               network. All commands which can be executed on the
//               BalzersGauge are implemented in this file.
//
// project :     Balzers gauge device server..
//
// $Author: pascal_verdier $
//
// $Revision: 1.20 $
// $Date: 2011/09/19 10:55:00 $
//
// SVN only:
// $HeadURL:  $
//
// CVS only:
// $Source: /cvsroot/tango-ds/Vacuum/BalzersGauge/BalzersGauge.cpp,v $
// $Log: BalzersGauge.cpp,v $
// Revision 1.20  2011/09/19 10:55:00  pascal_verdier
// Relays are now managed in TPG class (not by gauge).
//
// Revision 1.19  2011/05/31 06:00:48  pascal_verdier
// Cosmetic changes.
//
// Revision 1.18  2011/02/28 09:15:33  pascal_verdier
// Clean up dev_status() method.
//
// Revision 1.17  2011/01/19 15:11:27  pascal_verdier
// Pb in state/status during initialization fixed.
//
// Revision 1.16  2010/12/07 10:13:50  pascal_verdier
// Initialize is now done by a thread.
//
// Revision 1.15  2010/10/13 12:51:18  pascal_verdier
// Pogo-7 compatibility
//
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include <tango.h>
#include <BalzersGauge.h>
#include <BalzersGaugeClass.h>

/*----- PROTECTED REGION END -----*/	//	BalzersGauge.cpp

/**
 *  BalzersGauge class description:
 *    This is device server is intended to drive a vacuum gauge connected
 *    to a Balzers TPG300 controller.
 */

//================================================================
//  The following table gives the correspondence
//  between command and method names.
//
//  Command name  |  Method name
//================================================================
//  State         |  dev_state
//  Status        |  dev_status
//  On            |  on
//  Off           |  off
//================================================================

//================================================================
//  Attributes managed are:
//================================================================
//  Pressure   |  Tango::DevDouble	Scalar
//  XLocation  |  Tango::DevDouble	Scalar
//  YLocation  |  Tango::DevDouble	Scalar
//================================================================

namespace BalzersGauge_ns
{
/*----- PROTECTED REGION ID(BalzersGauge::namespace_starting) ENABLED START -----*/

	//	static initializations

	/*----- PROTECTED REGION END -----*/	//	BalzersGauge::namespace_starting

//--------------------------------------------------------
/**
 *	Method      : BalzersGauge::BalzersGauge()
 *	Description : Constructors for a Tango device
 *                implementing the classBalzersGauge
 */
//--------------------------------------------------------
BalzersGauge::BalzersGauge(Tango::DeviceClass *cl, string &s)
 : PressureGauge(cl, s.c_str())
{
	/*----- PROTECTED REGION ID(BalzersGauge::constructor_1) ENABLED START -----*/

	init_device();

	/*----- PROTECTED REGION END -----*/	//	BalzersGauge::constructor_1
}
//--------------------------------------------------------
BalzersGauge::BalzersGauge(Tango::DeviceClass *cl, const char *s)
 : PressureGauge(cl, s)
{
	/*----- PROTECTED REGION ID(BalzersGauge::constructor_2) ENABLED START -----*/

	init_device();

	/*----- PROTECTED REGION END -----*/	//	BalzersGauge::constructor_2
}
//--------------------------------------------------------
BalzersGauge::BalzersGauge(Tango::DeviceClass *cl, const char *s, const char *d)
 : PressureGauge(cl, s, d)
{
	/*----- PROTECTED REGION ID(BalzersGauge::constructor_3) ENABLED START -----*/

	init_device();

	/*----- PROTECTED REGION END -----*/	//	BalzersGauge::constructor_3
}

//--------------------------------------------------------
/**
 *	Method      : BalzersGauge::delete_device()
 *	Description : will be called at device destruction or at init command
 */
//--------------------------------------------------------
void BalzersGauge::delete_device()
{
	DEBUG_STREAM << "BalzersGauge::delete_device() " << device_name << endl;
	/*----- PROTECTED REGION ID(BalzersGauge::delete_device) ENABLED START -----*/

	//	Delete device's allocated object
    delete[] attr_XLocation_read;
    delete[] attr_YLocation_read;
    delete[] attr_Pressure_read;
    if (Tpg300ds!=NULL)
      delete Tpg300ds;

	/*----- PROTECTED REGION END -----*/	//	BalzersGauge::delete_device

	if (Tango::Util::instance()->is_svr_shutting_down()==false  &&
		Tango::Util::instance()->is_device_restarting(device_name)==false &&
		Tango::Util::instance()->is_svr_starting()==false)
	{
		//	If not shutting down call delete device for inherited object
		PressureGauge_ns::PressureGauge::delete_device();
	}
}

//--------------------------------------------------------
/**
 *	Method      : BalzersGauge::init_device()
 *	Description : will be called at device initialization.
 */
//--------------------------------------------------------
void BalzersGauge::init_device()
{
	DEBUG_STREAM << "BalzersGauge::init_device() create device " << device_name << endl;
	/*----- PROTECTED REGION ID(BalzersGauge::init_device_before) ENABLED START -----*/

    lower_limit_map.clear();
    upper_limit_map.clear();

    attr_XLocation_read = new Tango::DevDouble[1];
    attr_YLocation_read = new Tango::DevDouble[1];
    attr_Pressure_read  = new Tango::DevDouble[1];

    init_done  = false;

	/*----- PROTECTED REGION END -----*/	//	BalzersGauge::init_device_before
	
	if (Tango::Util::instance()->is_svr_starting() == false  &&
		Tango::Util::instance()->is_device_restarting(device_name)==false)
	{
		//	If not starting up call init device for inherited object
		PressureGauge_ns::PressureGauge::init_device();
	}

	//	Get the device properties from database
	get_device_property();
	
	/*----- PROTECTED REGION ID(BalzersGauge::init_device) ENABLED START -----*/


	//	Create proxy on controller
	Tpg300ds = new Tango::DeviceProxy(tpg300DeviceName);

	
	//	And add this gauge on controller
	Tango::DevVarStringArray sa;
	vector<string>	params;
	params.push_back(gaugeName);
	params.push_back(filterTime);
	params.push_back(device_name);

	Tango::DeviceData argin;
	argin << params;
	Tpg300ds->command_inout("AddGauge", argin);
	
	/*----- PROTECTED REGION END -----*/	//	BalzersGauge::init_device
}

//--------------------------------------------------------
/**
 *	Method      : BalzersGauge::get_device_property()
 *	Description : Read database to initialize property data members.
 */
//--------------------------------------------------------
void BalzersGauge::get_device_property()
{
	/*----- PROTECTED REGION ID(BalzersGauge::get_device_property_before) ENABLED START -----*/

	xLocation = 0.0;
    yLocation = 0.0;

	/*----- PROTECTED REGION END -----*/	//	BalzersGauge::get_device_property_before


	//	Read device properties from database.
	Tango::DbData	dev_prop;
	dev_prop.push_back(Tango::DbDatum("Tpg300DeviceName"));
	dev_prop.push_back(Tango::DbDatum("GaugeName"));
	dev_prop.push_back(Tango::DbDatum("FilterTime"));
	dev_prop.push_back(Tango::DbDatum("XLocation"));
	dev_prop.push_back(Tango::DbDatum("YLocation"));

	//	is there at least one property to be read ?
	if (dev_prop.size()>0)
	{
		//	Call database and extract values
		if (Tango::Util::instance()->_UseDb==true)
			get_db_device()->get_property(dev_prop);
	
		//	get instance on BalzersGaugeClass to get class property
		Tango::DbDatum	def_prop, cl_prop;
		BalzersGaugeClass	*ds_class =
			(static_cast<BalzersGaugeClass *>(get_device_class()));
		int	i = -1;

		//	Try to initialize Tpg300DeviceName from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  tpg300DeviceName;
		else {
			//	Try to initialize Tpg300DeviceName from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  tpg300DeviceName;
		}
		//	And try to extract Tpg300DeviceName value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  tpg300DeviceName;

		//	Try to initialize GaugeName from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  gaugeName;
		else {
			//	Try to initialize GaugeName from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  gaugeName;
		}
		//	And try to extract GaugeName value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  gaugeName;

		//	Try to initialize FilterTime from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  filterTime;
		else {
			//	Try to initialize FilterTime from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  filterTime;
		}
		//	And try to extract FilterTime value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  filterTime;

		//	Try to initialize XLocation from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  xLocation;
		else {
			//	Try to initialize XLocation from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  xLocation;
		}
		//	And try to extract XLocation value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  xLocation;

		//	Try to initialize YLocation from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  yLocation;
		else {
			//	Try to initialize YLocation from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  yLocation;
		}
		//	And try to extract YLocation value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  yLocation;

	}

	/*----- PROTECTED REGION ID(BalzersGauge::get_device_property_after) ENABLED START -----*/


	/*----- PROTECTED REGION END -----*/	//	BalzersGauge::get_device_property_after
}

//--------------------------------------------------------
/**
 *	Method      : BalzersGauge::always_executed_hook()
 *	Description : method always executed before any command is executed
 */
//--------------------------------------------------------
void BalzersGauge::always_executed_hook()
{
	DEBUG_STREAM << "BalzersGauge::always_executed_hook()  " << device_name << endl;
	/*----- PROTECTED REGION ID(BalzersGauge::always_executed_hook) ENABLED START -----*/

	if (init_done==false)
	{
    	try
    	{
			//	Check if initialized
			Tango::DeviceData argin;
			argin << gaugeName;
			Tango::DeviceData argout = Tpg300ds->command_inout("IsInitialized", argin);
			bool	init;
			argout >> init;
			init_done = init;				
    	}
    	catch (Tango::DevFailed &e)
    	{
			//Tango::Except::print_exception(e);
	 		throw e;
    	}
	}

	/*----- PROTECTED REGION END -----*/	//	BalzersGauge::always_executed_hook
}

//--------------------------------------------------------
/**
 *	Method      : BalzersGauge::read_attr_hardware()
 *	Description : Hardware acquisition for attributes
 */
//--------------------------------------------------------
void BalzersGauge::read_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "BalzersGauge::read_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(BalzersGauge::read_attr_hardware) ENABLED START -----*/

	//	Add your own code here

	/*----- PROTECTED REGION END -----*/	//	BalzersGauge::read_attr_hardware
}

//--------------------------------------------------------
/**
 *	Read attribute Pressure related method
 *	Description: Measured pressure on device.
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void BalzersGauge::read_Pressure(Tango::Attribute &attr)
{
	DEBUG_STREAM << "BalzersGauge::read_Pressure(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(BalzersGauge::read_Pressure) ENABLED START -----*/
    if(get_state() == Tango::ON || get_state() == Tango::ALARM)  {
        try  {
        Tango::DeviceData argin, argout;

        Tango::DevDouble pressure;
        argin << gaugeName;
        argout = Tpg300ds->command_inout("GetGaugePressure", argin);
        argout >> pressure;
        *attr_Pressure_read = pressure;
        attr.set_value(attr_Pressure_read);
        attr.set_quality(Tango::ATTR_VALID);
        }
        catch (Tango::DevFailed &e) {
        DEBUG_STREAM << "Failed to get gauge pressure." << endl;
        cerr << device_name  << "Failed to get gauge pressure." << endl;
        Tango::Except::print_exception(e);
        }
    }
    else
    {
        attr.set_quality(Tango::ATTR_INVALID);
    }

    /*----- PROTECTED REGION END -----*/	//	BalzersGauge::read_Pressure
}
//--------------------------------------------------------
/**
 *	Read attribute XLocation related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void BalzersGauge::read_XLocation(Tango::Attribute &attr)
{
	DEBUG_STREAM << "BalzersGauge::read_XLocation(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(BalzersGauge::read_XLocation) ENABLED START -----*/

    *attr_XLocation_read = xLocation;
    attr.set_value(attr_XLocation_read);

    /*----- PROTECTED REGION END -----*/	//	BalzersGauge::read_XLocation
}
//--------------------------------------------------------
/**
 *	Read attribute YLocation related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void BalzersGauge::read_YLocation(Tango::Attribute &attr)
{
	DEBUG_STREAM << "BalzersGauge::read_YLocation(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(BalzersGauge::read_YLocation) ENABLED START -----*/

    *attr_YLocation_read = yLocation;
    attr.set_value(attr_YLocation_read);

    /*----- PROTECTED REGION END -----*/	//	BalzersGauge::read_YLocation
}

//--------------------------------------------------------
/**
 *	Method      : BalzersGauge::add_dynamic_attributes()
 *	Description : Create the dynamic attributes if any
 *                for specified device.
 */
//--------------------------------------------------------
void BalzersGauge::add_dynamic_attributes()
{
	/*----- PROTECTED REGION ID(BalzersGauge::add_dynamic_attributes) ENABLED START -----*/

    //	Add your own code to create and add dynamic attributes if any

    /*----- PROTECTED REGION END -----*/	//	BalzersGauge::add_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Command State related method
 *	Description: This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
 *
 *	@returns State Code
 */
//--------------------------------------------------------
Tango::DevState BalzersGauge::dev_state()
{
	DEBUG_STREAM << "BalzersGauge::State()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(BalzersGauge::dev_state) ENABLED START -----*/

    Tango::DevState	argout;

    try
    {
        Tango::DeviceData my_argin, my_argout;

        my_argin << gaugeName;
        my_argout = Tpg300ds->command_inout("GetGaugeState", my_argin);
        my_argout >> argout;
    }
    catch(Tango::DevFailed &f)
    {
        string	err(f.errors[0].desc);
        cout << err << endl;
        set_status(err);
        argout = Tango::UNKNOWN;
    }

    /*----- PROTECTED REGION END -----*/	//	BalzersGauge::dev_state
	set_state(argout);    // Give the state to Tango.
	if (argout!=Tango::ALARM)
		Tango::DeviceImpl::dev_state();
	return get_state();  // Return it after Tango management.
}
//--------------------------------------------------------
/**
 *	Command Status related method
 *	Description: This command gets the device status (stored in its <i>device_status</i> data member) and returns it to the caller.
 *
 *	@returns Status description
 */
//--------------------------------------------------------
Tango::ConstDevString BalzersGauge::dev_status()
{
	DEBUG_STREAM << "BalzersGauge::Status()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(BalzersGauge::dev_status) ENABLED START -----*/

    string status;
    try
    {
        Tango::DeviceData argin;
        argin << gaugeName;
        Tango::DeviceData	argout = Tpg300ds->command_inout("GetGaugeStatus", argin);
        argout >> status;
    }
    catch(Tango::DevFailed &f)
    {
        status = f.errors[0].desc;
    }

    /*----- PROTECTED REGION END -----*/	//	BalzersGauge::dev_status
	set_status(status);               // Give the status to Tango.
	return Tango::DeviceImpl::dev_status();  // Return it.
}
//--------------------------------------------------------
/**
 *	Command On related method
 *	Description: Sets the gauge ON.
 *
 */
//--------------------------------------------------------
void BalzersGauge::on()
{
	DEBUG_STREAM << "BalzersGauge::On()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(BalzersGauge::on) ENABLED START -----*/

    Tango::DeviceData argin;
    argin << gaugeName;
    try {
        Tpg300ds->command_inout("On", argin);
    }
    catch (Tango::DevFailed &e) {
        DEBUG_STREAM << "Failed to set gauge ON." << endl;
        cerr << device_name  << "Failed to set gauge ON." << endl;
        Tango::Except::print_exception(e);
    }

    /*----- PROTECTED REGION END -----*/	//	BalzersGauge::on
}
//--------------------------------------------------------
/**
 *	Command Off related method
 *	Description: Sets the gauge OFF.
 *
 */
//--------------------------------------------------------
void BalzersGauge::off()
{
	DEBUG_STREAM << "BalzersGauge::Off()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(BalzersGauge::off) ENABLED START -----*/

    Tango::DeviceData argin;
    argin << gaugeName;
    try {
        Tpg300ds->command_inout("Off", argin);
    }
    catch (Tango::DevFailed &e) {
        DEBUG_STREAM << "Failed to set gauge OFF." << endl;
        cerr << device_name  << "Failed to set gauge OFF." << endl;
        Tango::Except::print_exception(e);
    }

    /*----- PROTECTED REGION END -----*/	//	BalzersGauge::off
}
//--------------------------------------------------------
/**
 *	Method      : BalzersGauge::add_dynamic_commands()
 *	Description : Create the dynamic commands if any
 *                for specified device.
 */
//--------------------------------------------------------
void BalzersGauge::add_dynamic_commands()
{
	/*----- PROTECTED REGION ID(BalzersGauge::add_dynamic_commands) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic commands if any
	
	/*----- PROTECTED REGION END -----*/	//	BalzersGauge::add_dynamic_commands
}

/*----- PROTECTED REGION ID(BalzersGauge::namespace_ending) ENABLED START -----*/

//+------------------------------------------------------------------
/**
 *	method:	BalzersGauge::gauge_code
 *
 *	description:	Returns the code of the gauge as defined in
 *                    the TPG300 documentation.
 *
 */
//+------------------------------------------------------------------
string BalzersGauge::gauge_code(string sensor)
{
    DEBUG_STREAM << "BalzersGauge::gauge_code(): entering... !" << endl;

    if(sensor == "A1")
      return("1");
    if(sensor == "A2")
      return("2");
    if(sensor == "B1")
      return("3");
    if(sensor == "B2")
      return("4");

    Tango::Except::throw_exception(
				     (const char *)"GAUGE_NAME_ERROR",
				     (const char *)err_msg[GAUGE_NAME_ERROR], 
				     (const char *)"BalzersGauge::gauge_code()");	
}

	/*----- PROTECTED REGION END -----*/	//	BalzersGauge::namespace_ending
} //	namespace
