# Project BalzersGauge

Cpp project

This is device server is intended to drive a vacuum gauge connected
to a Balzers TPG300 controller.


## Cloning

```
git clone git@gitlab.esrf.fr:accelerators/Vacuum/BalzersGauge
```

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies 

* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.
  

#### Toolchain Dependencies 

* C++11 compliant compiler.
* CMake 3.0 or greater is required to perform the build. 
  

### Build

Instructions on building the project.

CMake example: 

```bash
cd project_name
mkdir build
cd build
cmake ../
make
```

Make example: 

```bash
cd project_name
make
```
